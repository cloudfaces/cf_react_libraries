import React from 'react';
import styles from './MainContainer.css';

const MainContainer = (props) => (
  <div id="main-container">
    {props.children}
  </div>
);

MainContainer.propTypes = {

};

export default MainContainer;
