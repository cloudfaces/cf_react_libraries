import React from 'react';
import styles from './Login.scss';
import MainContainer from '../MainContainer';
import MainNav from '../MainNav';
import Tabs, {TabPane} from 'rc-tabs';
import ScrollableInkTabBar from 'rc-tabs/lib/ScrollableInkTabBar';
import TabContent from 'rc-tabs/lib/TabContent';
// import Nouislider from 'react-nouislider';
import {
  Container,
  Box,
  Page,
  Alert,
  Button,
  Header,
  WaterHeaterSlider,
  Footer,
  Input,
  Label,
  MenuComponent,
  Ribbon,
  Title,
  Toggle,
  TextComp,
  Checkbox,
  Time,
  Slider,
  Span
} from '../../../../';

let callback = function () {};
class Login extends React.Component {
  static propTypes = {}
  constructor(props) {
    super(props);

  }

  render = () => {
    return (
        <Container id="login" alignItems="flex-center" center column>
          <Container fit column>
            <Title type="white center">Log In</Title>
            <Container fit column>
              <Input onChange={this.test} type="email" placeholder="Email"/>
              <Input type="password" placeholder="Password"/>
            </Container>
          </Container>

          <Container fit column center>
            <Box column alignItems="cenetr">
              <TextComp type="white text">Forgot password?</TextComp>
            </Box>
            <Box fit column>
              <Button type="bobbie-default">Sign In</Button>
              <Box column alignItems="center">
                <TextComp type="white text">or</TextComp>
              </Box>
              <Button type="success">Register new account</Button>
            </Box>
          </Container>

          <a href="http://seemelissa.com" id="info">
            <Box className="footer" justifyContent="flex-end" center column>
              <TextComp type="white">
                <img src={require('./img/icon-arrow.png')}/>
                <Span type="white">Learn more about Bobbie</Span>
              </TextComp>
            </Box>
          </a>
        </Container>
    )
  };
}

export default Login;
