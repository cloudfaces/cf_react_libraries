import React from 'react';
import cssModule from 'react-css-modules';
import styles from './App.css';
import MainContainer from '../MainContainer'
import MainNav from '../MainNav'

const App = (props) => (
  <MainContainer>
    <MainNav/>
    {props.children}
  </MainContainer>
);

App.propTypes = {

};

export default App;
