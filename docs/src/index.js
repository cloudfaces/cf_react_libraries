/* global CFPersistentStorage */
import React from 'react';
import {render} from 'react-dom';
import App from './components/App';
import HomePage from './components/HomePage';

import Login from './components/Login';
import Registration from './components/Registration';


import ComponentsPage from './components/ComponentsPage';
import {Router, Route, browserHistory, IndexRoute} from 'react-router';

import style from './style/main.scss';

render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={HomePage} />
      <Route path="components" component={ComponentsPage} />
       <Route path="login" component={Login} />
       <Route path="registration" component={Registration} />
       
    </Route>
  </Router>
  ,
  document.getElementById('app')
);    