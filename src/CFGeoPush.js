function CFGeoPush(client, fenceUrl, fenceLogUrl) {
	if (!(this instanceof CFGeoPush)) {
		return new CFGeoPush(client, fenceUrl, fenceLogUrl);
	}

	Object.assign(this, {
		isInit: true,
		client: client,
		fence_url: typeof fenceUrl != 'undefined' ? fenceUrl : '',
		fenceLogUrl: typeof fenceLogUrl != 'undefined' ? fenceLogUrl : ''
	})
}

CFGeoPush.prototype = {
    init: function() {
        this.events();
        this.loadFences();
    },

	loadFences: function() {
		let url = this.fence_url + this.client.app_id + '/';

        this.client.get(url).then(fences => {
        	if (typeof fences != 'undefined' && fences) {
                this.addRemoveFences(fences);
            }
        });
	},
	
	addRemoveFences: function(fences) {
		if (typeof fences !== 'undefined' && Array.isArray(fences) && fences.length > 0) {
			// Native bridge
            CFLocation.getFences(function(items) {
            	items.map(item => {
            		CFLocation.removeFence(item.id);
            	});

            	fences.map(item => {
            		let fence = {
                        lat: item.fence_lat,
                        long: item.fence_long,
                        id: item.id,
                        radius: item.fence_radius
                    };

                    CFLocation.addFence(fence);
            	});


                const appId = this.client.app_id;
                const ain = (new CFDevice(this.client)).getDeviceAin();
                const os = isMobile.Android() ? 'android' : (isMobile.iOS() ? 'ios' : 'web');

                if (this.fenceLogUrl) {
                	CFLocation.setFencingUrl(this.fenceLogUrl(appId, ain, os));
                }
                
                CFLocation.getCurrentPosition(function(pos){
                });
            });
        }
	},

	events: function() {
		if (this.isInit) {
            this.isInit = false;
        }
	}
}

export default CFGeoPush;