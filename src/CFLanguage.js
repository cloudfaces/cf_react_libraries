function CFLanguage(client, listId, appLanguages) {	
	if (!(this instanceof CFLanguage)) {
		return new CFLanguage(client, listId, appLanguages);
	}

	Object.assign(this, {
		client: client,
		list_id: listId,
		app_languages: appLanguages
	})
}

CFLanguage.prototype = {
	translations: function() {
		return new Promise((resolve, reject) => {
			let url = 'project/id/%s/lists/%s/items/'
				.replace('%s', this.client.app_id)
				.replace('%s', this.list_id);

			this.client.get(url).then(results => {
				resolve(this.format(results));
			}).catch(e => {
				reject([]);
			})
		})
	},

	format: function(results) {
		let languages = {};
		for (var i = 0; i < this.app_languages.length; i++) {
			const lang = this.app_languages[i];
			let translations = {};
			for (var j = 0; j < results.length; j++) {		
				translations[results[j]['inx'].trim()] = (results[j][lang]).trim();
			}		

			languages[lang] = {translations: translations};
		}
		return languages;	
	}
}

export default CFLanguage;