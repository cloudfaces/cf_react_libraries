import axios from 'axios';
import CFException from './CFException';
import CFDevice from './CFDevice';

function CFCore(options) {
    if (!(this instanceof CFCore)) {
        return new CFCore(options);
    }

    Object.assign(this, options);
}

/**
 * Retrieves the items of a list
 */
CFCore.prototype.getItems = function(listId, listParentId, params) {
    if (!listId) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/lists/%s/items/'
        .replace('%s', this.app_id)
        .replace('%s', listId);

    params = typeof params == 'object' ? params : {};

    if (typeof listParentId != 'undefined' && listParentId) {
        params.parent = listParentId;
    }

    return this.get(url, params);
}

CFCore.prototype.getItemWithRelations = function(listId, listParentId, params) {
    if (!listId) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    params = typeof params == 'object' ? params : {};
    params.fetch_relations = true;

    return this.getItems(listId, listParentId, params);
}

CFCore.prototype.getOption = function(key) {
    if (typeof key == 'undefined' && key) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }
    
    let url = 'project/id/%s/options/option/%s/'
            .replace('%s', this.app_id)
            .replace('%s', key);

    return this.get(url);
}

CFCore.prototype.customSql = function(customSqlId, params) {
    if (!customSqlId) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/customsql/%s/'
        .replace('%s', this.app_id)
        .replace('%s', customSqlId);

    params = typeof params == 'object' ? params : {};    

    params.custom_sql = true;

    return this.query(url, 'post', params)
}

/**
 * Relations
 */
CFCore.prototype.getRelations = function(relId, listId, itemId) {
    if (!relId || !itemId || !listId) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/relations/relationid/%s/listid/%s/itemid/%s/'
        .replace('%s', this.app_id)
        .replace('%s', relId)
        .replace('%s', listId)
        .replace('%s', itemId);

    return this.get(url);
}

CFCore.prototype.addRelation = function(relationId, list1Id, item1Id, list2Id, item2Id) {
    if (!relationId || !list1Id || !item1Id || !list2Id || !item2Id) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/relation/listid/%s/itemid/%s/'
        .replace('%s', this.app_id)
        .replace('%s', list1Id)
        .replace('%s', item1Id);

    let params = {
        content_type: 'formdata',
        relation_id: relationId,
        list_id_2: list2Id,
        item_id_2: item2Id
    };

    return this.post(url, params);
}

CFCore.prototype.deleteRelation = function(relationId, list1Id, item1Id, list2Id, item2Id) {
    if (!relationId || !list1Id || !item1Id || !list2Id || !item2Id) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/relation/listid/%s/itemid/%s/'
        .replace('%s', this.app_id)
        .replace('%s', list1Id)
        .replace('%s', item1Id);

    let params = {
        relation_id: relationId,
        list_id_2: list2Id,
        item_id_2: item2Id
    }

    return this.delete(url, params);
}

/**
 * Specific item
 */
CFCore.prototype.getItem = function(id, listId, params) {
    if (!listId || !id) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/lists/%s/items/%s/'
        .replace('%s', this.app_id)
        .replace('%s', listId)
        .replace('%s', id);

    return this.get(url, params);
}

CFCore.prototype.addItem = function(listId, listParentId, params)  {
    let url = 'project/id/%s/lists/%s/items/'
        .replace('%s', this.app_id)
        .replace('%s', listId);

    if (typeof listParentId != 'undefined' && listParentId) {
        params.parent = listParentId;
    }

    return this.put(url, params)
}

CFCore.prototype.updateItem = function(id, listId, listParentId, params)  {
    if (!id || !listId) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    params = typeof params == 'object' ? params : {};
    params.id = id;

    if (typeof listParentId != 'undefined' && listParentId) {
        params.parent = listParentId;
    }

    let url = 'project/id/%s/lists/%s/items/'
        .replace('%s', this.app_id)
        .replace('%s', listId);

    return this.put(url, params)
}

CFCore.prototype.deleteItem = function(id, listId)  {
    if (!id || !listId) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }
    
    let url = 'project/id/%s/list/%s/item/%s/'
        .replace('%s', this.app_id)
        .replace('%s', listId)
        .replace('%s', id);

    return this.delete(url)
}

/**
 * Http method
 */
CFCore.prototype.get = function(url, params) {
    return this.query(url, 'get', params)
}

CFCore.prototype.post = function(url, params) {
    return this.query(url, 'post', params)
}

CFCore.prototype.put = function(url, params) {
    return this.query(url, 'put', params)
}

CFCore.prototype.delete = function(url, params) {
    return this.query(url, 'delete', params);
}

/**
 * Push notification
 */
CFCore.prototype.setRegisterPush = function(params) {
    let url = 'project/id/%s/register/push/notifications/'
            .replace('%s', this.app_id)
    return this.post(url, params);
} 

CFCore.prototype.getRegisterPush = function(params) {
    let url = 'get/registered/push/notifications/'    
    return this.post(url, params);
} 

CFCore.prototype.deleteRegisterPush = function(params) {
    let url = 'delete/registered/push/notifications/'    
    return this.post(url, params);
} 

CFCore.prototype.getNotifications = function(params) {
    params = typeof params == 'object' ? params : {};
    params.project_id = this.app_id;

    if (!params.hasOwnProperty('ain') || !params.hasOwnProperty('user_id')) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'notifications/';

    return this.get(url, params);
}

CFCore.prototype.getNotificationCategories = function() {
    const params = {
        id: this.app_id
    };  

    if (!params.hasOwnProperty('id')) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/push/categories/'
            .replace('%s', this.app_id);

    return this.get(url, params); 
}

CFCore.prototype.getNotificationSettings = function(ain) {
    if (!ain) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/ain/%s/push/settings/'
            .replace('%s', this.app_id)
            .replace('%s', ain);

    return this.get(url); 
}
CFCore.prototype.editNotificationSettings = function(params) {
    params = typeof params == 'object' ? params : {};
    params.project_id = this.app_id;

    if (!params.hasOwnProperty('ain')) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let url = 'project/id/%s/ain/%s/push/settings/'
            .replace('%s', this.app_id)
            .replace('%s', params.ain);

    return this.put(url, params); 
}

CFCore.prototype.registerAppWithToken = function(token, ain) {
    if (!token || !ain) {
        return Promise.reject(new CFException('api', 'invalid_parameters', 'Invalid parameters!'));
    }

    let params = {
        os: isMobile.Android() ? 'android' : 'ios',
        token: token,
        device_id: (new CFDevice(this)).getDeviceId(),
        app: this.app_hash,
        ain: ain
    };
    let url = 'tokens/';

    return this.put(url, params);
}

CFCore.prototype.setAnalyticData = function(pageId) {
    if (!pageId) {
        return Promise.reject(new CFException('api', 'page_id_is_required', 'Page ID is required!'));
    }

    let params = {
        device_id: (new CFDevice(this)).getDeviceId(),
        project_id: this.app_id,
        page_id: pageId
    };
    let url = 'stats/';

    return this.put(url, params);
}

export default CFCore;