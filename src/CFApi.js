/* global CFHttpRequest, isMobile */
import axios from 'axios';
import {urlParams} from './helpers/utils';
import CFException from './CFException';
import CFCore from './CFCore';
import CFLanguage from './CFLanguage';
var qs = require('qs');

function CFApi(opts) {
  if (!(this instanceof CFApi)) {
    return new CFApi(opts);
  }

  Object.assign(this, opts, {
    debug: (window.location.href.indexOf('http') >= 0)
  });

  if (typeof this.base_uri == 'undefined') {
    throw new CFException('api', 'base_uri_required', 'Base url is required!');
  }

  if (typeof this.app_id == 'undefined') {
    throw new CFException('api', 'app_id_required', 'App id is required!');
  }

  if (typeof this.app_hash == 'undefined') {
    throw new CFException('api', 'app_hash_required', 'App hash is required!');
  }

  if (typeof this.user_token == 'undefined') {
    this.setToken('');
  }

  CFCore.call(this);
}

CFApi.prototype = Object.create(CFCore.prototype);

CFApi.prototype.query = function(url, method, params = {}) {
  let self = this;
  return new Promise((resolve, reject) => {   
    let hasSetToken = false;
    if (typeof this.user_access_token != 'undefined') {
      hasSetToken = true;
      url = url + (url.indexOf('?')>=0 ? '&' : '?') + "access_token=" + this.user_access_token;
    } else if (typeof this.client_access_token != 'undefined') {
      hasSetToken = true;
      url = url + (url.indexOf('?')>=0 ? '&' : '?') + "access_token=" + this.client_access_token;      
    }

    let dataParams = params;
    const hasCustomSql = typeof dataParams.custom_sql != 'undefined' ? true : false;
    delete dataParams.custom_sql;

    // // check if mobile and use CFHttpRequest
    if ((isMobile.Android() || isMobile.iOS()) && !hasCustomSql ) {
      let request = new CFHttpRequest();
      request.Method = method.toUpperCase();

      if (request.Method == "GET") {
          request.Url = this.base_uri + url + (url.indexOf('?')>=0 ? '&' : '?') + qs.stringify(dataParams);
      } else {
          request.Url = this.base_uri + url;
          request.Body = qs.stringify(dataParams);
      }

      request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      request.send(function(result) {                
          if (typeof result === 'undefined') {
            reject(new CFException('api', 'no_internet', 'No internet'));
            return;
          } else {              
              if (result.Code == 200) {
                let data = JSON.parse(result.Body); 
                if (data.Status === 'Success' && typeof data.Data != 'undefined') {
                    resolve(data.Data);
                } else {
                  if (data.ErrorCode == 401) {                    
                    if (typeof self.user_access_token != 'undefined') {
                      if (typeof self.refresh_token != 'undefined') {
                        let refresh_token = self.refresh_token; 
                        self.refresh_token = '';
                        self.requestRefreshToken(refresh_token).then(response => {
                          self.user_access_token = response.access_token
                          self.refresh_token = response.refresh_token;
                          self.query(url, method, dataParams).then(results => resolve(results)).catch(e => reject(e));
                        });                       
                      }
                    } else {
                      self.requestCredentials().then(response => {                        
                        self.client_access_token = response.access_token;
                        self.query(url, method, dataParams).then(results => resolve(results)).catch(e => reject(e));
                      });                      
                    }

                    return;
                  } else {
                    if (data.ErrorCode == 'wrong_token') {
                      reject(new CFException('api', 'expired_token', 'Expired token'));
                    } else if (data.hasOwnProperty('message_long')) {
                      reject(new CFException('api', data.ErrorCode, data.message_long));
                    } else {
                      reject(new CFException('api', data.ErrorCode, data.ErrorMessage));            
                    } 

                    return;
                  }       
                }
                return
              } else {
                  reject(new CFException('api', 'invalid_response', 'Invalid Response'));
                return;
              }
          }
      });

      return;
    }

    let formData = null;
    if (typeof params.content_type != 'undefined' && params.content_type == 'formdata') {
      delete params.content_type;
      formData = new FormData();
      for (let i in dataParams) {      
         formData.append(i, dataParams[i]);
      }
    } else {
      formData = method == 'post' ||(hasCustomSql && method == 'post' && typeof dataParams.grant_type == 'undefined') 
        ? dataParams 
        : qs.stringify(dataParams)
    }


    method = method.toLowerCase();
    let ajaxParams = {
      baseURL: this.base_uri,
      method: method,      
      data: formData,
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    };

    if (method == 'get') {
      ajaxParams.params = params
      ajaxParams.paramsSerializer = function(params) {
        return qs.stringify(params)
      }
    }

    if (params instanceof FormData) {
        ajaxParams.cache = false;
        ajaxParams.contentType = false;
        ajaxParams.processData = false;
    }

    axios(url, ajaxParams).then(response => {           
      let data = response.data;      

      // Upon an empty request
      if (!data) {
        reject(new CFException('api', 'empty_result', 'Empty Result'));
        return;
      }

      if (typeof data !== 'object') {
        reject(new CFException('api', 'invalid_json', 'Invalid JSON'));
        return;
      }
      
      if (data.Status === 'Success' && typeof data.Data != 'undefined') {
          resolve(data.Data);
      } else {
        if (data.ErrorCode == 401) {                    
          if (typeof this.user_access_token != 'undefined') {
            if (typeof this.refresh_token != 'undefined') {
              let refresh_token = this.refresh_token; 
              this.refresh_token = '';
              this.requestRefreshToken(refresh_token).then(response => {
                this.user_access_token = response.access_token
                this.refresh_token = response.refresh_token;
                this.query(url, method, dataParams).then(results => resolve(results)).catch(e => reject(e));
              });                       
            }
          } else {
            this.requestCredentials().then(response => {                        
              this.client_access_token = response.access_token;
              this.query(url, method, dataParams).then(results => resolve(results)).catch(e => reject(e));
            });                      
          }

          return;
        } else {
          if (data.ErrorCode == 'wrong_token') {
            reject(new CFException('api', 'expired_token', 'Expired token'));
          } else if (data.hasOwnProperty('message_long')) {
            reject(new CFException('api', data.ErrorCode, data.message_long));
          } else {
            reject(new CFException('api', data.ErrorCode, data.ErrorMessage));            
          } 

          return;
        }       
      }
    }).catch(e => { reject(e); }) 
  })  
}

CFApi.prototype.requestCredentials = function(){
  let url, args;

  url = 'oauth/token/';
  args = {
        grant_type:'client_credentials',
        client_id: this.app_id,
        client_secret: this.app_hash
    };

  return this.query(url, 'post', args);
}

CFApi.prototype.requestRefreshToken = function(refresh_token){             
  let url, args;

  url = 'oauth/token/';
  args = {
        grant_type:'refresh_token',
        client_id: this.app_id,
        client_secret: this.app_hash,
        refresh_token: refresh_token
    };

  return this.query(url, 'post', args);
}

CFApi.prototype.requestPasswordToken = function(username, password){            
  let url, args;

  url = 'oauth/token/';
  args = {
        grant_type:'password',
        client_id: this.app_id,
        client_secret: this.app_hash,
        username:username,
        password:password
    };

  return this.query(url, 'post', args);
}

CFApi.prototype.setToken = function(token) {
  this.user_token = token;
}

export default CFApi;