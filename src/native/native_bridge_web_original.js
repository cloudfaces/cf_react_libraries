function CFHttpRequest() {
	this.Method = "GET";
	this.Body = "";
	this.Url = "http://localhost";
	this.Headers = {};
	
	this.setRequestHeader = function(header, value) {
		this.Headers[header] = value;
	};
	
	this.send = function(callback) {
		callback.call(this, null);
	}
};

var CFVariableStorage = {

  valueExists : function valueExists(key) {    
    return false;
  },

  writeValue : function writeValue(key, value) {

  },
  
  readValue : function readValue(key) {
  	return null;
  }
};


var CFNavigation = {
	navigate : function navigate(page, context) {
		var $form = $( '<form />' ),
			$input = $( '<input type="hidden" name="context" />' ).val( context );
		$form.attr( 'action', page );
		$form.attr( 'method', 'post' );
		$form.append( $input );
		$form.appendTo('body').submit();
	},
	
	getNavigationContext : function getNavigationContext() {
		return null;
	},

	navigateBack : function navigateBack(result) {

	}
};

var CFMenuNavigation = {
	navigate : function navigate(page, context) {
		return CFNavigation.navigate( page, context );
	},
	
	getNavigationContext : function getNavigationContext() {
		return CFNavigation.getNavigationContext()
	},

	navigateBack : function navigateBack(result) {
		return CFNavigation.navigateBack( result );
	}
};

var CFPersistentStorage = {

  valueExists : function valueExists(key) {    
    return false;
  },

  writeValue : function writeValue(key, value) {

  },
  
  readValue : function readValue(key) {
  	return null;
  }
};


var CFScanner = {
	scanBarCode : function scanBarCode(callback) {
		callback.call(null, "dummy 123456");
	},
};

$( document ).trigger( 'cf-ready' );