    function CFHeaderButton() {
    this.text;
    this.textColor;
    this.icon;
    this.function;
}

function CFContentView() {
    this.type;
    this.color;
    this.text;
}

function CFPageHeader() {
    this.backgroundColor;
    this.titleColor;
    this.backgroundImage;
    this.titleImage;
    this.rightButton;
    this.leftButton;
    this.contentView;
    this.statusBarColor;

}

function CFPage() {
    this.uniqueId;
    this.type = "html";
    this.title;
    this.pageHeader;
}

function CFTabItem() {
    this.title;
    this.icon;
}

function CFTabPage() {
    this.type = "tab";
    this.tabItems = [];
}

CFTabPage.prototype = new CFPage();

function CFHttpRequest() {
    this.Method = "GET";
    this.Body = "";
    this.Url = "http://localhost";
    this.BinaryFile = "";
    this.Headers = {};

    this.setRequestHeader = function(header, value) {
        this.Headers[header] = value;
    };

    this.send = function(callback) {
        var json = JSON.stringify(this);
        var _callback = function(res) { callback.call(this, res); };
        NativeBridge.call("CFNetwork.sendHttpRequest", [this], _callback);
    };

    this.sendBinary = function(callback) {
        var json = JSON.stringify(this);
        var _callback = function(res) { callback.call(this, res); };
        NativeBridge.call("CFNetwork.sendHttpRequestBinary", [this], _callback);
    };
};

function CFLocation() {
    this.long = 0.0;
    this.lat = 0.0;
}

function CFGeoFence() {
    this.lat = 0.0;
    this.long = 0.0;
    this.id = "GeoFence";
    this.radius = 1; // meter
}

var NativeBridge = {
    callbacksCount: 1,
    callbacks: {},

    // Automatically called by native layer when a result is available
    resultForCallback: function resultForCallback(callbackId, resultArray) {
        try {
            var callback = NativeBridge.callbacks[callbackId];
            if (!callback) return;

            callback.apply(null, resultArray);
        } catch (e) { alert(e) }
    },

    // Use this in javascript to request native objective-c code
    // functionName : string (I think the name is explicit :p)
    // args : array of arguments
    // callback : function with n-arguments that is going to be called when the native code returned
    call: function call(functionName, args, callback) {

        var hasCallback = callback && typeof callback == "function";
        var callbackId = hasCallback ? NativeBridge.callbacksCount++ : 0;

        if (hasCallback)
            NativeBridge.callbacks[callbackId] = callback;

        var iframe = document.createElement("IFRAME");
        iframe.setAttribute("src", "js-command:" + functionName + ":" + callbackId + ":" + encodeURIComponent(JSON.stringify(args)));
        // For some reason we need to set a non-empty size for the iOS6 simulator...
        iframe.setAttribute("height", "1px");
        iframe.setAttribute("width", "1px");
        document.documentElement.appendChild(iframe);
        iframe.parentNode.removeChild(iframe);
        iframe = null;


        //Debug console log
        console = new Object();
        console.log = function(log) {

            NativeBridge.call("CFRuntime.log", [log, 1], null);

            var iframe = document.createElement("IFRAME");
            iframe.setAttribute("src", "ios-log:" + log);
            document.documentElement.appendChild(iframe);
            iframe.parentNode.removeChild(iframe);
            iframe = null;
        }
        console.error = function(log) {
            NativeBridge.call("CFRuntime.log", [log, 2], null);
            var iframe = document.createElement("IFRAME");
            iframe.setAttribute("src", "ios-log-error:" + log);
            document.documentElement.appendChild(iframe);
            iframe.parentNode.removeChild(iframe);
            iframe = null;
        }

        console.warn = function(log) {
            NativeBridge.call("CFRuntime.log", [log, 3], null);

            var iframe = document.createElement("IFRAME");
            iframe.setAttribute("src", "ios-log-warn:" + log);
            document.documentElement.appendChild(iframe);
            iframe.parentNode.removeChild(iframe);
            iframe = null;
        }
        console.info = function(log) {
            NativeBridge.call("CFRuntime.log", [log, 4], null);
            var iframe = document.createElement("IFRAME");
            iframe.setAttribute("src", "ios-log-info:" + log);
            document.documentElement.appendChild(iframe);
            iframe.parentNode.removeChild(iframe);
            iframe = null;
        }
        console.debug = function(log) {
            NativeBridge.call("CFRuntime.log", [log, 5], null);
            var iframe = document.createElement("IFRAME");
            iframe.setAttribute("src", "ios-log-debug:" + log);
            document.documentElement.appendChild(iframe);
            iframe.parentNode.removeChild(iframe);
            iframe = null;
        }

    },

    //callBackResult : null,
    callSync: function callSync(functionName, args) {

        var callBackResult = null;

       // console.log('call ' + functionName);

        callBack = function(r) {
         //   console.log('callback ' + r);
            callBackResult = r;
        }

        NativeBridge.call(functionName, args, callBack);
        //console.log('return ' + callBackResult);

        return callBackResult;

    },
};

var CFVariableStorage = {
    valueExists: function valueExists(key) {
        return NativeBridge.callSync("CFVariableStorage.valueExists", [key], null);
    },

    writeValue: function writeValue(key, value) {
        NativeBridge.call("CFVariableStorage.writeValue", [key, value], null);
    },

    readValue: function readValue(key) {
        return NativeBridge.callSync("CFVariableStorage.readValue", [key], null);
    }
};

var CFPersistentStorage = {
    valueExists: function valueExists(key) {
        return NativeBridge.callSync("CFPersistentStorage.valueExists", [key], null);
    },

    writeValue: function writeValue(key, value) {
        NativeBridge.call("CFPersistentStorage.writeValue", [key, value], null);
    },

    readValue: function readValue(key) {
        return NativeBridge.callSync("CFPersistentStorage.readValue", [key], null);
    }
};

var CFNavigation = {
    navigate: function navigate(page, context) {
        NativeBridge.call("CFNavigation.navigate", [page, context], null);
    },

    getNavigationContext: function getNavigationContext() {
        return NativeBridge.callSync("CFNavigation.getNavigationContext", null);
    },

    navigateBack: function navigateBack(result) {
        NativeBridge.call("CFNavigation.navigateBack", [result], null);
    },

    navigateAndAddToBackStack: function navigate(page, context) {
        NativeBridge.call("CFNavigation.navigateAndAddToBackStack", [page, context], null);
    },

    navigateBackFromStack: function navigate(page, context) {
        NativeBridge.call("CFNavigation.navigateBackFromStack", [page, context], null);
    },

    navigateToRoot: function navigateToRoot(page, context) {
        return NativeBridge.call("CFNavigation.navigateToRoot", [page, context], null);
    },

};

var CFMenuNavigation = {
    navigate: function navigate(page, context) {
        NativeBridge.call("CFMenuNavigation.navigate", [page, context], null);
    },

    getNavigationContext: function getNavigationContext() {
        return NativeBridge.callSync("CFMenuNavigation.getNavigationContext", null);
    },

    navigateBack: function navigateBack(result) {
        NativeBridge.call("CFMenuNavigation.navigateBack", [result], null);
    }
};

var CFScanner = {
    scanBarCode: function scanBarCode(callback) {
        NativeBridge.call("CFScanner.scanBarCode", null, callback);
    },
};

var CFFacebook = {
    // callback: function(token)
    login: function login(perms, callback) {
        NativeBridge.call("CFFacebook.login", [perms], callback);
    },

    getToken: function getToken(callback) {
        return NativeBridge.call("CFFacebook.getToken", null, callback);
    },

    isLoggedIn: function isLoggedIn(callback) {
        return NativeBridge.call("CFFacebook.isLoggedIn", null, callback);
    },

    logout: function logout(callback) {
        return NativeBridge.call("CFFacebook.logout", null, callback);
    },
    appInvates: function logout(appLinkUrl, previewImageUrl, callback) {
        return NativeBridge.call("CFFacebook.appInvites", [appLinkUrl, previewImageUrl], callback);
    },
};

var CFRuntime = {

    loadApp: function loadApp(value) {
        NativeBridge.call("CFRuntime.loadApp", [value], null);
    },

    log : function(message, level) {
       // console.log(message, level);
        NativeBridge.call("CFRuntime.log", [message, level], null);
    },

    resetLocalAppFiles: function() {
        NativeBridge.call("CFRuntime.resetLocalAppFiles", null, null);
    },

    restartLocalApp: function() {
        NativeBridge.call("CFRuntime.restartLocalApp", null, null);
    },

    getLocalFiles: function(callback) {
        NativeBridge.call("CFRuntime.getLocalFiles", null, callback);
    },

    deleteLocalFile: function(fileUrl) {
        NativeBridge.call("CFRuntime.deleteLocalFile", [fileUrl], null);
    },

    addLocalFile: function(targetType, dstUrl, srcUrl, callback) {
        NativeBridge.call("CFRuntime.addLocalFile", [targetType, dstUrl, srcUrl], callback);
    },

    findPage: function(pageName, callback) {
        NativeBridge.call("CFRuntime.findPage", [pageName], callback);
    },
    findCurrentPage: function(callback) {
        NativeBridge.call("CFRuntime.findCurrentPage", null, callback);
    },
    updatePage: function(pageName, page) {
        NativeBridge.call("CFRuntime.updatePage", [pageName, page], null);
    },

    setCustomContent: function(type) {
        NativeBridge.call("CFRuntime.setCustomContent", [type], null);
    },

    rightToLeftLayout: function(value, callback) {
        NativeBridge.call("CFRuntime.rightToLeftLayout", [value], callback);
    },

    setOrientation: function(setOrientation, callback) {
        return NativeBridge.call("CFRuntime.setOrientation", [setOrientation], callback);
    },
    unsetOrientation: function(callback) {
        return NativeBridge.call("CFRuntime.unsetOrientation", null, callback);
    }
};

var CFPushNotifications = {
    getToken: function getToken() {
        return NativeBridge.callSync("CFPushNotifications.getToken", null);
    },
};

var CFCalendarEvents = {

    // calback is function(success:boolean)
    createEvent: function(title, startYear, startMonth, startDay, startHour, startMinute, durationMinutes, callback) {
        return NativeBridge.call("CFCalendarEvents.createEvent", [title, startYear, startMonth, startDay, startHour, startMinute, durationMinutes], callback);
    },

    getCalendars: function(callback) {
        return NativeBridge.call("CFCalendarEvents.getCalendars", null, callback);
    },

    insertEvent: function(calendarIds, beginTime, endTime, title, description, reminderTime, location, callback) {
        return NativeBridge.call("CFCalendarEvents.insertEvent", [calendarIds, beginTime, endTime, title, description, reminderTime, location], callback);
    },

    updateEvent: function(event_id, beginTime, endTime, title, description, reminderTime, location, callback) {
        return NativeBridge.call("CFCalendarEvents.updateEvent", [event_id, beginTime, endTime, title, description, reminderTime, location], callback);
    },

    deleteEvent: function(event_id, callback) {
        return NativeBridge.call("CFCalendarEvents.deleteEvent", [event_id], callback);
    },

};

var CFLocation = {

    // callback is
    getCurrentPosition: function(callback) {
        return NativeBridge.call("CFLocation.getCurrentPosition", null, callback);
    },

    startTracking: function(url) {
        return NativeBridge.call("CFLocation.startTracking", [url], null);
    },
    startTrackingAndSendLocationToServer: function(url, seconds) {
        return NativeBridge.call("CFLocation.startTrackingAndSendLocationToServer", [url, seconds], null);
    },
    stopTracking: function() {
        return NativeBridge.call("CFLocation.stopTracking", null, null);
    },

    getLocationQueue: function(callback) {
        return NativeBridge.call("CFLocation.getLocationQueue", null, callback);
    },

    getFences: function(callback) {
        return NativeBridge.call("CFLocation.getFences", null, callback);
    },

    addFence: function(fence) {
        return NativeBridge.call("CFLocation.addFence", [fence]);
    },

    removeFence: function(fenceId) {
        return NativeBridge.call("CFLocation.removeFence", [fenceId]);
    },

    setFencingUrl: function(url) {
        return NativeBridge.call("CFLocation.setFencingUrl", [url]);
    },

    setData: function(callback) {
        return NativeBridge.call("CFLocation.setData", null, callback);

    }
};

var CFNotification = {

    addObserverForName: function addObserverForName(notificationName, callback) {
        NativeBridge.call("CFNotification.addObserverForName", [notificationName], callback);
    },

    removeObserverForName: function removeObserverForName(notificationName) {
        NativeBridge.call("CFNotification.removeObserverForName", [notificationName], null);
    },

    postNotificationWithName: function postNotificationWithName(value) {
        NativeBridge.call("CFNotification.postNotificationWithName", [value], null);
    },
    };

    var CFPictureChooser = {

    capturePicture: function capturePicture(source, resultType, callback) {
        NativeBridge.call("CFPictureChooser.capturePicture", [source, resultType], callback);
    },

    getCapturedPictures: function getCapturedPictures(callback) {
        NativeBridge.call("CFPictureChooser.getCapturedPictures", null, callback);
    },

    getPictureData: function getPictureData(imageUrl, callback) {
        NativeBridge.call("CFPictureChooser.getPictureData", [imageUrl], callback);
    },

    deletePicture: function deletePicture(imageUrl) {
        NativeBridge.call("CFPictureChooser.deletePicture", [imageUrl], null);
    },

    deleteAllPictures: function deleteAllPictures() {
        NativeBridge.call("CFPictureChooser.deleteAllPictures", null, null);
    },
};

var CFIdentity = {

    getDeviceIdentifier: function getDeviceIdentifier(callback) {
        NativeBridge.call("CFIdentity.getDeviceIdentifier", null, callback);
    }
};

var CFShare = {

    shareString: function(text) {
        return NativeBridge.call("CFShare.shareString", [text], null);
    }
};


var CFExternal = {

    showExternalContent: function(title, url) {
        return NativeBridge.call("CFExternal.showExternalContent", [title, url], null);
    },

    openUrlInSafari: function (url) {
        return NativeBridge.call("CFExternal.openUrlInSafari", [url], null);
    }
};

var CFPrivacy = {

    getContacts: function(callback) {
        return NativeBridge.call("CFPrivacy.getContacts", null, callback);
    },

    getContactsDataByIds: function(contactId, callback) {
        return NativeBridge.call("CFPrivacy.getContactsDataByIds", [contactId], callback);
    },
    checkPermission: function(type, callback) {
        callback(true)
    },

    requestPermission: function(type, callback) {
        callback(true)
    }
};

var CFBadge = {

    update: function(badgeNumber, callback) {
        return NativeBridge.call("CFBadge.update", [badgeNumber], callback);
    },

    get: function(callback) {
        return NativeBridge.call("CFBadge.get", null, callback);
    }
};

var CFDialog = {

    show: function(type, title, actionsAsString, dataAsString, callback) {
        return NativeBridge.call("CFDialog.show", [type, title, actionsAsString, dataAsString], callback);

    }
};

var CFHeader = {

    updateToggle: function(date, visibility, callback) {
        return NativeBridge.call("CFHeader.updateToggle", [date, visibility], callback);
    },
    updateHeader: function(title, callback) {
        return NativeBridge.call("CFHeader.updateHeader", [title], callback);
    }
};


var CFPayPal = {

    singlePayment: function callback(items, details, callback) {
        return NativeBridge.call("CFPayPal.singlePayment", [items, details], callback);
    }
};


var CFAnalytics = {

    registerPage: function(pageName, callback) {
        return NativeBridge.call("CFAnalytics.registerPage", [pageName]);
    },

    sendEvent: function(category, action, label, value) {
        return NativeBridge.call("CFAnalytics.sendEvent", [category, action, label, value]);

    },

    sendProduct: function(transactionId, name, category, price, quantity, currencyCode, sku) {
        return NativeBridge.call("CFAnalytics.sendProduct", [transactionId, name, category, price, quantity, currencyCode, sku]);

    },

    sendTransaction: function(transactionId, affiliation, revenue, tax, shipping, currencyCode) {
        return NativeBridge.call("CFAnalytics.sendTransaction", [transactionId, affiliation, revenue, tax, shipping, currencyCode]);

    }

};

var CFNetwork = {

    getConnectionType: function(callback) {
        return NativeBridge.call("CFNetwork.getConnectionType", null, callback);
    },

    getCurrentWifi: function(callback) {
        return NativeBridge.call("CFNetwork.getCurrentWifi", null, callback);
    },

};

var SmartLink = {

    start: function(ssid, password, timeout, callback) {
        return NativeBridge.call("SmartLink.start", [ssid, password, timeout], callback);
    }
};
var CFBeacon = {

    beacons: function(callback) {
        return NativeBridge.call("CFBeacon.beacons", callback);
    },

    apiRequestBeaconLogin: function(url, method, body, callback) {
        return NativeBridge.call("CFBeacon.apiRequestBeaconLogin", [url, method, body], callback);
    },


    apiRequestBeaconLogout: function(url, method, callback) {
        return NativeBridge.call("CFBeacon.apiRequestBeaconLogout", [url, method], callback);
    },


    getBeacons: function(url, method, body, callback) {
        return NativeBridge.call("CFBeacon.getBeacons", [url, method], callback);
    },
    getBeaconProperty: function(url, method, callback) {
        return NativeBridge.call("CFBeacon.getBeaconProperty", [url, method], callback);
    },

    getGroups: function(url, method, body, callback) {
        return NativeBridge.call("CFBeacon.getGroups", [url, method], callback);
    },

    getCampaign: function(url, method, body, callback) {
        return NativeBridge.call("CFBeacon.getCampaign", [url, method], callback);
    },

    sendBeaconNotification: function(url, method, body, callback) {
        return NativeBridge.call("CFBeacon.apiRequestBeaconLogin", [url, method, body], callback);
    },

    beaconTerminate: function(callback) {
        return NativeBridge.call("CFBeacon.beaconStop", callback);
    }
};

var CFNativeComponent = {
    wirelessSettings: function(callback) {
        return NativeBridge.call("CFNativeComponent.wirelessSettings", callback);
    },
    vibratePhone: function(ms, callback) {
        return NativeBridge.call("CFNativeComponent.vibratePhone", [ms], callback);
    }
};

var CFDocuments = {
    getAll: function(callback) {
        return NativeBridge.call("CFDocuments.getAll", null, callback);
    }
};

CFRuntime.findCurrentPage(function (page) {
    var event = new CustomEvent("cf-ready", { "detail": page });
    document.dispatchEvent(event);
});

// callback to the native code
NativeBridge.call("CFNotification.postNotificationWithName", ["CFLoaded"], null);