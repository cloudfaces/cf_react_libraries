
var CFVars = {
	navigationContext: '',
	variable: {},
	triggered: false
};

var CFNavigation = {
	navigate: function navigate(page, context) {
		CFVars.navigationContext = context;
		var event = new CustomEvent("cf-navigate", { "detail": page });
		document.dispatchEvent(event);
	},

	getNavigationContext: function getNavigationContext() {
		return CFVars.navigationContext;
	},

	navigateBack: function navigateBack(result) {
		return false;
	},
	navigateAndAddToBackStack: function (page, context) {
		CFVars.navigationContext = context;
		var event = new CustomEvent("cf-navigate", { "detail": page });
		document.dispatchEvent(event);
	},
	navigateToRoot: function (page, context) {
		CFVars.navigationContext = context;
		var event = new CustomEvent("cf-navigate", { "detail": page });
		document.dispatchEvent(event);
	}
};

var CFMenuNavigation = {
	SmartConfig: function () {
		return false;
	},

	navigate: function navigate(page, context) {
		return CFNavigation.navigate(page, context);
	},

	getNavigationContext: function getNavigationContext() {
		return CFNavigation.getNavigationContext();
	},

	navigateBack: function navigateBack(result) {
		return CFNavigation.navigateBack(result);
	}
};

function CFHttpRequest() {
	this.Method = "GET";
	this.Body = "";
	this.Url = "http://localhost";
	this.Headers = {};
	
	this.setRequestHeader = function(header, value) {
		this.Headers[header] = value;
	};
	
	this.send = function(callback) {
		callback.call(this, null);
	}
}

var CFPersistentStorage = {
	valueExists: function valueExists(key) {
		return localStorage.getItem(key) != null;
	},

	writeValue: function writeValue(key, value) {
		localStorage.setItem(key, value);
	},

	readValue: function readValue(key) {
		return localStorage.getItem(key);
	}
};

var CFVariableStorage = {
	valueExists: function valueExists(key) {
		return false;
	},

	writeValue: function writeValue(key, value) {
		CFVars.variable[key] = value;
	},

	readValue: function readValue(key) {
		return CFVars.variable[key];
	}
};

// function CFLocation() {
// 	this.long = 0.0;
// 	this.lat = 0.0;
// }

var CFLocation = {
	// callback function: function(position)
	getCurrentPosition: function (callback) {
		return false;
	},

	startTracking: function (url) {
		return false;
	},

	stopTracking: function () {
		return false;
	},

	getLocationQueue: function (callback) {
		return false;
	},

	getFences: function (callback) {
		callback([1, 2]);
	},

	addFence: function (fence) {
		return false;
	},

	removeFence: function (fenceId) {
		return false;
	},

	setFencingUrl: function (url) {
		return false;
	}
};

function CFGeoFence() {
	this.lat = 0.0;
	this.long = 0.0;
	this.id = "GeoFence"; // each fence has a unique identifier which is used to remove or replace a fence
	this.radius = 1; // meter, the minimum may be quite larger, typically about 100m
}

var CFPushNotifications = {
	getToken: function () {
		return "69c89540 61a33c8d 072f8ead b8771c89 030deac4 72d3ce5d ef37625d b61e0c8b";
	}
}

var CFNotification = {
	listeners:[],
	addObserverForName: function addObserverForName(notificationName, callback) {
		CFNotification.listeners.push({name:notificationName, callback:callback});
		return false;
	},
	removeObserverForName: function removeObserverForName(notificationName) {
		CFNotification.listeners = CFNotification.listeners.filter(function(listener){
			return listener.name !== notificationName
		})
		return false;
	},
	postNotificationWithName: function postNotificationWithName(notificationName) {
		CFNotification.listeners.forEach(function(listener){
			if(listener.name == notificationName){
				listener.callback();
			}
		})
		return false;
	}
};

var CFRuntime = {
	findCurrentPage: function (callback) {
		var QueryString = function () {
			// This function is anonymous, is executed immediately and the return value is
			// assigned to QueryString!
			var query_string = {};
			var query = window
				.location
				.search
				.substring(1);
			var vars = query.split("&");
			for (var i = 0; i < vars.length; i++) {
				var pair = vars[i].split("=");
				// If first entry with this name
				if (typeof query_string[pair[0]] === "undefined") {
					query_string[pair[0]] = decodeURIComponent(pair[1]);
					// If second entry with this name
				} else if (typeof query_string[pair[0]] === "string") {
					var arr = [
						query_string[pair[0]],
						decodeURIComponent(pair[1])
					];
					query_string[pair[0]] = arr;
					// If third or later entry with this name
				} else {
					query_string[pair[0]].push(decodeURIComponent(pair[1]));
				}
			}
			return query_string;
		}();
		if ("page" in QueryString)
			;
		callback(QueryString.page);
	},
	log: function(data){
		console.log(data);
	}
}

var SmartPlug = {
	addDevice(device) {
		var mac = device.mac;
		var pid = device.pid;
		var did = device.did;
		var type = device.deviceType;
		var name = device.name;
		var pass = device.password;
		var key = device.controlKey;
		var id = device.controlId;
		var ip = "";
		return true;
	},
	setThirdPartyLogin: function (id, callback) {
		var response = JSON.stringify({ loginsession: "10442615c3b6a923e551c11b6c47b11b", getMsg: "ok", userid: "10442615c3b6a923e551c11b6c47b11b" })
		setTimeout(function () {
			callback(response);
		}, 300)
	},

	wifiConfig: function (wifi, pass, callback) {
		var response = JSON.stringify({ status: "success" })
		setTimeout(function () {
			callback(response);
		}, 300);
	},
	allDevices: function (callback) {
		var response = [
			{
				"mac": "34:ea:34:e3:fa:53",
				"pid": "00000000000000000000000033270000",
				"did": "0000000000000000000034ea34e3fa53",
				"state": 1,
				"led": 1,
				"type": 10035,
				"name": "My Smart Plug",
				"lock": 0,
				"controlKey": "fc5474040c3c84341ca44c0c14fc9cc4",
				"access_key": "fc5474040c3c84341ca44c0c14fc9cc4",
				"password": "742421",
				"subdevice": 0,
				"appliance_id": 1
			}
		]; callback(response);
	},
	queryDeviceState(did) {
		return Math.floor(Math.random() * 3);
	},
	getProfile: function (string, callback) {
		var response = JSON.stringify({ status: "success" })
		setTimeout(function () {
			callback(response);
		}, 300);
	},
	setParam: function (string1, string2, did, callback) {
		var response = JSON.stringify({ status: "success" })
		setTimeout(function () {
			callback(response);
		}, 300);
	},
	getParam: function (param, did, callback) {
		var response = JSON.stringify({ status: "success" })
		setTimeout(function () {
			callback(response);
		}, 300);
	},
	startProbe: function () {
		console.log("Start probe.");
	},
	devProbleListener: function () {
		console.log("Start probe dev listener.")
	},
	stopProbe: function () {
		console.log("Stop probe.");
	},
	stopWifiConfig: function () {
		console.log("Stop wifi config.")
	},
	scriptDownload: function (id, callback) {
		console.log("Start Download")
		callback('script')
	}
};

CFRuntime.findCurrentPage(function (page) {
	var event = new CustomEvent("cf-ready", { "detail": page });
	document.dispatchEvent(event);
});