var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

(function() {
    document.addEventListener("DOMContentLoaded", function(event) {
        var src, cssClass;

        console.log("DOM fully loaded and parsed");

        var scrElement = document.createElement('script');
        if (isMobile.Android()) {           
            src      = 'native/native_bridge_android.js';
            cssClass = 'android';
        } else if (isMobile.iOS()) {
            src      = 'native/native_bridge_ios.js';
            cssClass = 'ios';
        } else {
            src      = 'native/native_bridge_web.js';
            cssClass = 'web';
        }

        scrElement.setAttribute('src', src);
        document.body.className = (document.body.className + ' ' + cssClass).trim();
        document.head.appendChild(scrElement);
    });
})();

function onTimeOut(){
    var event = new CustomEvent("cf-wifi-timeout");
    document.dispatchEvent(event);
}
function onCompleted(){
    var event = new CustomEvent("cf-wifi-completed");
    document.dispatchEvent(event);
}
function onLinked(args){
    // alert(JSON.stringify(args))
}
function smartConfigEspResult(args){    
    var event = new CustomEvent("cf-esp-wifi-completed");
    document.dispatchEvent(event);
}

var loadFirstTime = true;

function onIsActive(){
    if( loadFirstTime ) { loadFirstTime = false; }
    else{
        var event = new CustomEvent("cf-resume", { "detail": "Resume" });
        document.dispatchEvent(event);
    }
}

function onIsInActive(){
    // if( loadFirstTime ) { loadFirstTime = false; }
    // else{
        var event = new CustomEvent("cf-suspend", { "detail": "Suspend" });
        document.dispatchEvent(event);
    //}
}