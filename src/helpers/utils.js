export function urlParams(a) { 
	return Object.keys(a).map(function(k) {
		return encodeURI(k) + '=' + encodeURI(a[k])
	}).join('&');
}

export function getRandomString() {
    var symbols = 'UXbxo5HVB2Y9isl4E1Mqgc7vKudIQe3FRyLaNWwzOhCnp08Pkm6DfTJrSjtZAG',
        str = new Date().getTime().toString(),
        i;

    while (str.length < 64) {
        str += symbols.charAt(Math.round(Math.random() * symbols.length));
    }

    return str;
}

export function deviceType() {
	if (navigator.userAgent.match(/Android/i)) {
		return 'android';
	} else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
		return 'ios';
	} else {
		return 'web';
	}
}