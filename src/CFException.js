function CFException(type, code, message) {
    this.isException = true;
    this.type = type;
    this.code = code;
    this.message = message;
}

/**
 * Static methods that check for exceptions.
 *
 * @type {object} e The (eventually) exception.
 * @type {string|null} type The type or code of the exception. Optional.
 */
CFException.prototype.isException = function(e, type) {
    // If it's not an object, it's not an exception
    if (typeof e != 'object')
        return false;

    // If it's an object, but it does not have isException, it's not one
    if (typeof e.isException == 'undefined')
        return false;

    // If no particular type is needed, it passes
    if (typeof type != 'string')
        return true;

    // Check type / type.code
    if (type.indexOf('.') != -1)
        return e.type + '.' + e.code == type;
    else
        return e.type == type;
}

export default CFException;