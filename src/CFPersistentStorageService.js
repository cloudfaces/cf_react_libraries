/* global CFPersistentStorage */
const CFPersistentStorageService = {
    get(key){
        if(CFPersistentStorage.valueExists(key)){
            return CFPersistentStorage.readValue(key);
        }else{
            throw new Error('No such key.')
        }   
    },

    exists(key){
        return CFPersistentStorage.valueExists(key);
    },

    set(key, value){
        CFPersistentStorage.writeValue(key, value);
    }
}

export default CFPersistentStorageService;