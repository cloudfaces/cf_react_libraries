import CFPersistentStorageService from './CFPersistentStorageService';
import qs from 'qs';
import {getRandomString} from './helpers/utils';

function CFDevice(client) {
    if (!(this instanceof CFDevice)) {
        return new CFDevice(client);
    }

    Object.assign(this, {
        client: client,
        device_code: '',
        device_ain: '',
        device_token: '',
        os: '',
        getAIN: ''
    })
}

CFDevice.prototype = {
    init: function() {
        // let getAINKey = this.client.app_id + '_getAIN';
        var device_code = this.getDeviceCode();
        var device_ain = this.getDeviceAin();

        // this.getAIN = CFPersistentStorageService.exists(getAINKey) && CFPersistentStorageService.get(getAINKey) !== '' ? CFPersistentStorageService.get(getAINKey) : '';
        /*if (this.getAIN !== 'yes') {
            CFPersistentStorageService.set(getAINKey, 'yes');*/

        if (!device_code || !device_ain) {
            return this.set();
        } else {
            this.device_code = device_code;
            this.device_ain = device_ain;

            return Promise.resolve({
                ain: device_ain,
                code: device_code
            });
        }
        /*} else {
            return Promise.resolve({
                ain: device_ain,
                code: device_code
            });
        }*/

        // register device ain
        if (typeof device_ain !== 'undefined' && device_ain !== '') {
            this.registerDeviceTokenAin(device_ain);
        }
    },

    set: function() {
        return new Promise((resolve, reject) => {
            var debug = typeof this.client.debug != 'undefined' ? this.client.debug : (window.location.href.indexOf('http') >= 0);
            let params = {
                os: this.phoneType(),
                project_id: this.client.app_id,
                project_hash: this.client.app_hash,
                debug: debug
            }
            let url = 'get_device_id/?'+qs.stringify(params);

            this.client.get(url).then(device => {
                this.device_code = device.code;
                this.device_ain = device.ain;
                CFPersistentStorageService.set(this.client.app_id + '_device_code', device.code);
                CFPersistentStorageService.set(this.client.app_id + '_device_ain', device.ain);
                CFPersistentStorageService.set('app_id',  this.client.app_id);
                CFPersistentStorageService.set('app_hash_url', this.client.base_uri + 'project/version/project_hash/' + this.client.app_hash +'/');

                resolve({
                    ain: device.ain,
                    code: device.code
                });
            }).catch(e => {
                reject({
                    ain: 0,
                    code: ''
                });
            });
        });
    },

    getDeviceCode: function() {
        var device_code = CFPersistentStorageService.exists(this.client.app_id + '_device_code') ? CFPersistentStorageService.get(this.client.app_id + '_device_code') : '';

        if (typeof device_code != 'undefined' && device_code != 'undefined' && device_code) {
            this.device_code = device_code;

            return device_code;
        }

        return 0;
    },

    getDeviceAin: function() {
        var device_ain = CFPersistentStorageService.exists(this.client.app_id + '_device_ain') ? CFPersistentStorageService.get(this.client.app_id + '_device_ain') : '';

        if (typeof device_ain != 'undefined' && device_ain != 'undefined' && device_ain) {
            this.device_ain = device_ain;

            return device_ain;
        }

        return 0;
    },

    getDeviceToken: function() {
        var device_token = CFPersistentStorageService.exists(this.client.app_id + '_device_token') ? CFPersistentStorageService.get(this.client.app_id + '_device_token') : '';

        if (typeof device_token != 'undefined' && device_token != 'undefined' && device_token) {
            this.device_token = device_token;

            return device_token;
        }

    },

    setDeviceId: function() {
        let deviceId = '';

        if (!CFPersistentStorageService.exists('device_id')) {
            deviceId = getRandomString();
            CFPersistentStorageService.set('device_id', deviceId);
        }

        return deviceId;
    },

    getDeviceId: function() {
        if (CFPersistentStorageService.exists('device_id')) {
            return CFPersistentStorageService.get('device_id');
        } else {
            return this.setDeviceId();
        }
    },

    phoneType: function() {
        if (navigator.userAgent.match(/Android/i)) {
            return 'android';
        } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
            return 'ios';
        } else {
            return 'web';
        }

        return "other";
    },

    registerDeviceTokenAin: function(ain) {
        /*if (this.client.debug) {
            let token = 'APA91bHQFZxOwrh3PUNHhoOnRGLsVmYFa6OCxrZ_vEmtF6reM2v4EHmtTZnbHxjuh8N0Tiss4alXydXxBuJqzNnDeiz8daoBuK7UhsadZVMfubfogCSaM45GUsjA0hUjktoPooMJaEGY';
            //Lyubo: APA91bHVOv2fszXMuU6Kx4g7syb9D5mfDd2LsEkGIl2L9CydjCBhbKE2pn1y7inj47hSRgEzzmuacLJF3BXddZNOq__EKP8qnCwG2Gf7BlyyB36MMtIdO3ojnE7F44RW7WbcJxCmJF61
            //Alex: APA91bHQFZxOwrh3PUNHhoOnRGLsVmYFa6OCxrZ_vEmtF6reM2v4EHmtTZnbHxjuh8N0Tiss4alXydXxBuJqzNnDeiz8daoBuK7UhsadZVMfubfogCSaM45GUsjA0hUjktoPooMJaEGY

            if (isMobile.iOS()) {
                token = '660a1ac471ed0ee29e7c566a7031d0d8df5683bcd675447ac6d127b25d65c596';
                //Ned: 1eb1c183ab516b630d0cc8dbd9bfa4e4f88260c46e0440678ebefdf1c6996a7b
                //Lyubo: 660a1ac471ed0ee29e7c566a7031d0d8df5683bcd675447ac6d127b25d65c596
            }

            if (token && ain) {
                this.client.registerAppWithToken(token.replace(/\s/g, ''), ain);
            }
        } else {*/
            // From native bridge
            let token = CFPushNotifications.getToken();

            if (token != '' && ain != '' && ain != 0 && token != ain) {
                this.client.registerAppWithToken(token.replace(/\s/g, ''), ain);
                CFPersistentStorageService.set(this.client.app_id + '_device_token', token.replace(/\s/g, ''));
            }
        // }
    }
}

export default CFDevice;