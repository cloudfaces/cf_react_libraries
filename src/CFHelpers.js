/* global isMobile */
import CFPersistentStorageService from './CFPersistentStorageService';
import qs from 'qs';

function CFHelpers(client) {
    if (!(this instanceof CFHelpers)) {
        return new CFHelpers(client);
    }

    Object.assign(this, {
        client: client
    })
}

CFHelpers.prototype = {
    clearStorage: function(key) {
        if (key) {
            CFPersistentStorageService.set(key, '');
        } else {
            CFPersistentStorageService.set('checkVersion', '');
            CFPersistentStorageService.set('promotions', '');
            CFPersistentStorageService.set('welcomeMessage', '');
            CFPersistentStorageService.set('getAIN', '');
            CFPersistentStorageService.set('device_code', '');
            CFPersistentStorageService.set('device_ain', '');
            CFPersistentStorageService.set('checkVersion', '');
            CFPersistentStorageService.set('checkedNotificationsfromPopup', '');
            CFPersistentStorageService.set(this.client.app_id + '_checkVersion', '');
            CFPersistentStorageService.set(this.client.app_id + '_promotions', '');
            CFPersistentStorageService.set(this.client.app_id + '_welcomeMessage', '');
            CFPersistentStorageService.set(this.client.app_id + '_getAIN', '');
            CFPersistentStorageService.set(this.client.app_id + '_device_code', '');
            CFPersistentStorageService.set(this.client.app_id + '_device_ain', '');
            CFPersistentStorageService.set(this.client.app_id + '_checkVersion', '');
            CFPersistentStorageService.set(this.client.app_id + '_checkedNotificationsfromPopup', '');
        }

        return true;
    },

    dateDBFormat: function(date) {
        var parts = (date).split(/[- :]/),
            new_date = new Date();

        for (var i in parts) parts[i] = parseInt(parts[i]);
        new_date = new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4]);

        return new_date;
    },

    twoDigits: function(d) {
        if (0 <= d && d < 10) return "0" + d.toString();
        if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
        return d.toString();
    },

    toMysqlFormat: function(date) {
        return date.getFullYear() + "-" + this.twoDigits(1 + date.getMonth()) + "-" + this.twoDigits(date.getDate()) + " " + this.twoDigits(date.getHours()) + ":" + this.twoDigits(date.getMinutes()) + ":" + this.twoDigits(date.getSeconds());
    },
    
    createThumb: function(timThumbUrl, image, width, height) {
        if (image) {
            let thumb = timThumbUrl + image;
            let params = {};
            if (typeof width != 'undefined' && width) {
                params['w'] = width;
            }
            if (typeof height != 'undefined' && height) {
                params['h'] = height;
            }

            return thumb + '&' + qs.stringify(params);
        } else {
            // reject(new CFException('api', 'image_required', 'Image is required!'));
            return '';
        }
    },

    getThumb(apiUrl, timThumbUrl, image) {
        let thumb = '';

        if (image) {
            const w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            const h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            const size = w/2;
            let imageSrc = apiUrl +'/'+ image;
            thumb = this.createThumb(timThumbUrl, imageSrc, size);
        }

        return thumb;
    },

    getHours: function(date) {
        return (date.getHours() < 10 ? '0' : '') + date.getHours();
    },

    getDate: function(date) {
        return (date.getDate() < 10 ? '0' : '') + date.getDate();
    },

    getMinutes: function(date) {
        return (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
    },

    getMonth: function(date) {
        return ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
    },

    showLoader: function() {
        // $('#loader').show();
    },

    hideLoader: function() {
        // $('#loader').hide();
    },

    cutText: function(text, bufferLen, allowedLen) {
        var buffer = bufferLen || 20,
            allowed_length = allowedLen || 50;

        if (typeof text != 'undefinde' && text) {
            if (text.length <= (allowed_length + buffer)) {
                return text;
            } else {
                var left = text.substr(0, allowed_length);
                var right = text.substr(allowed_length, text.length).split(/\s+/g)[0];
                return left + (typeof right != 'undefined' && right ? right : '') + '...';
            }
        }

        return '';
    },

    navigateTo: function(coords) {
        var navigateUrl = 'http://maps.google.com/maps?daddr=' + coords;
        if (isMobile.iOS()) {
            navigateUrl = 'maps://maps.google.com/maps?daddr=' + coords;
        }

        location.href = navigateUrl;
    },

    phoneCall(phone, key) {
        return new Promise((resolve, reject) => {
            this.showLoader();
            if (typeof phone != 'undefined' && phone != '') {
                this.hideLoader();
                resolve(phone);
                window.location = 'tel: ' + phone;
            } else {
                this.client.getOption(key).then(phone => {
                    var nativePhone = phone;
                    resolve(phone);
                    this.hideLoader();
                    window.location = 'tel: ' + nativePhone;
                }).catch(e => { reject(e) });
            }
        });             
    },

    getUserData: function() {
    	return new Promise((resolve, reject) => {
	        if (CFPersistentStorageService.exists(this.client.app_id + '_userData')) {
	            if (CFPersistentStorageService.get(this.client.app_id + '_userData') != null) {
	                if (CFPersistentStorageService.get(this.client.app_id + '_userData') != '') {
	                    resolve(JSON.parse(CFPersistentStorageService.get(this.client.app_id + '_userData')));
	                } else {
	                    reject('');

	                }
	            } else {
	                reject('');
	            }
	        } else {
	            reject('');
	        }
        });  
    },

    getUserToken: function() {
    	if (CFPersistentStorageService.exists(this.client.app_id + '_userData')) {
            if (CFPersistentStorageService.get(this.client.app_id + '_userData') != null) {
                if (CFPersistentStorageService.get(this.client.app_id + '_userData') != '') {
                    let userData = JSON.parse(CFPersistentStorageService.get(this.client.app_id + '_userData'));
                    return userData.token;
                } else {
                    return '';

                }
            } else {
                return '';
            }
        } else {
            return '';
        }       
    },

    checkLogin: function() {
    	return new Promise((resolve, reject) => {		
    		let refreshIntervalId = setInterval(function() {
	            if (this.getUserData() != '') {
	                clearInterval(refreshIntervalId);
	                resolve();
	            }
	            reject();
	        }, 1000);
    	});       
    },

    checkWebSite: function() {    	
        if (!isMobile.Android() && !isMobile.iOS()) {
            return true;
        } else {
            return false;
        }
    }
};


export default CFHelpers;