function CFImportExport(client, listId) {	
	if (!(this instanceof CFImportExport)) {
		return new CFImportExport(client, listId);
	}

	Object.assign(this, {
		client: client,
		list_id: listId
	})
}

CFImportExport.prototype = {
	export: function() {
		const url = 'export';
		const params = {
			project_id: this.client.app_id,
			list: this.list_id,
			token: this.client.user_token
		};

		return this.client.get(url, params);
	},

	import: function(data) {
		const url = 'import';
		const params = {
			project_id: this.client.app_id,
			list: this.list_id,
			token: this.client.user_token,
			data: JSON.stringify(data)
		};

		return this.client.post(url, params);
	},

	// Not implemented yet in v2
	importRelations: function(relationId, data) {
		const url = 'relations/import';
		const params = {
			project_id: this.client.app_id,
			relation: relationId,
			token: this.client.user_token,
			data: JSON.stringify(data)
		};

		return this.client.post(url, params);
	},

	delete: function(items = 'all') {
		const url = 'delete';
		const params = {
			project_id: this.client.app_id,
			list: this.list_id,
			token: this.client.user_token,
			items: items == 'all' ? 'all' : JSON.stringify(items)
		};

		return this.client.delete(url, params);
	}
}

export default CFImportExport;