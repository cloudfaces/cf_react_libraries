import CFDevice from './CFDevice';
import CFException from './CFException';

function CFAnalytics(client, pageId = 0, pageTitle = "") {
	if (!(this instanceof CFAnalytics)) {
		return new CFAnalytics(client, pageId, pageTitle);
	}

	Object.assign(this, {
		client: client,		
		analytics_url: 'stats/',
		project_id: client.app_id,
		page_id: pageId,
		page_title: pageTitle,
		debug: client.debug ? client.debug : (window.location.href.indexOf('http') >= 0)
	})
}

CFAnalytics.prototype = {    
    set: function() {
        const appInstanceId = (new CFDevice(this.client)).getDeviceCode();

        if (typeof appInstanceId == 'undefined' || !appInstanceId || appInstanceId == 'undefined') {
            return Promise.reject(new CFException('api', 'device_code_required', 'Device code is required!'));
        }

        return this.send(appInstanceId);
    },

    send: function(appInstanceId) {
    	let params = {
            device_id: appInstanceId,
            project_id: this.project_id,
            page_id: this.page_id,
            page_title: this.page_title,
            debug: this.debug
        }
        let url = this.analytics_url;

        return this.client.put(url, params);
    }
};

export default CFAnalytics;