function CFTransition(client, options) {
	if (!(this instanceof CFTransition)) {
		return new CFTransition(client, options);
	}

	let container = typeof options.container != 'undefined' && options.container ? options.container : null;
	let animationDuration = typeof options.animation_duration != 'undefined' && options.animation_duration ? options.animation_duration : '500'; 
	let durationModifier = typeof options.duration_modifier != 'undefined' && options.duration_modifier ? options.duration_modifier : 1;

	Object.assign(this, {
		client: client,
		container: container,
		transitions: {},
		animation_duration: animationDuration,
		duration_modifier: durationModifier,
		available_directions: [ 'X', 'Y', 'Z' ],
		user_directions: {}
	});
}

CFTransition.prototype = {
	getBrowserEnginePrefix: function () {
		let transition, vendor, i;
		transition = "Transition";
		vendor = ["Moz", "Webkit", "Khtml", "O", "ms"];
		i = 0;
		while (i < vendor.length) {
			if (typeof document.body.style[vendor[i] + transition] === "string") {
				return { css: vendor[i] };
			}
			i++;
		}
		return false;
	},

	start: function() {
		return this.transition('start');
	},
	
	reset: function() {
		return this.transition('reset');
	}
};

export default CFTransition;