import CFHelpers from './CFHelpers';

function CFCaching(client, listId, itemId, inx, time, alive, userToken) {
	if (!(this instanceof CFCaching)) {
		return new CFCaching(client, listId, itemId, inx, time, alive, userToken);
	}

	const cfHelpers = new CFHelpers(client);

	Object.assign(this, {
		client: client,		
		list_id: listId || 0,
        inx: inx || '_trash_',
        time: time || 0,
        item_id: itemId || '',
        user_token: userToken || cfHelpers.getUserToken(),
        storage: alive ? CFPersistentStorage : CFVariableStorage
	})
}

CFCaching.prototype = {
	getCMS: function(params = {}) {
		return this.client.getItems(this.list_id, 0, params);
    },

    getElementCMS: function(params = {}) {
        return this.client.getItem(this.item_id, this.list_id, params);
    },

    needUpdate: function() {
        return true;
    },

    getFirst: function() {    	
    	return new Promise((resolve, reject) => {
    		if (!this.needUpdate() && this.storage.valueExists(this.inx) && this.storage.readValue(this.inx)) {
	            let data = JSON.parse(this.storage.readValue(this.inx));
	            if (data.length > 0) {
	                resolve(data[0]);
	            } else {
	                resolve([]);
	            }
	        } else {
	        	let params = {
	        		sort_by: "id",
	        		sort_type: "asc",
	        		limit: 1
	        	};        	
	        	this.getCMS(params).then(items => {	   
	        		if (typeof items != 'undefined' && items.length) {
	        			let item = items[0];	        			
                        this.storage.writeValue(this.inx, JSON.stringify(items));
                        Offline.set(this.inx, JSON.stringify(items))
                        resolve(item);
	                } else {
	                    resolve([]);
	                    // Offline.showPopupNoInternet(CloudFaces.Language.translate('need_login'), CloudFaces.Language.translate('no_internet_understand'))
	                }
	        	}).catch(e => {
	        		if (typeof e.code != 'undefined' && e.code == 'no_internet') {
	        			resolve(Offline.get(this.inx));
	        		} else {
	        			// Offline.showPopupNoInternet(CloudFaces.Language.translate('need_login'), CloudFaces.Language.translate('no_internet_understand'))
		        		reject(e);
		        	}
	        	});
	        }
    	});        
    },

    getLast: function() {
    	return new Promise((resolve, reject) => {
	        if (!this.needUpdate() && this.storage.valueExists(this.inx) && this.storage.readValue(this.inx)) {
	            let data = JSON.parse(this.storage.readValue(this.inx));
	            if (data.length > 0) {
	                resolve(data[0]);
	            } else {
	                resolve([]);
	            }
	        } else {
	        	let params = {
	        		sort_by: "id",
	        		sort_type: "desc",
	        		limit: 1
	        	};
	            this.getCMS(params).then(items => {	        		
	        		if (typeof items != 'undefined' && items.length) {
                        let item = items[0];
                        this.storage.writeValue(this.inx, JSON.stringify(items));
                        Offline.set(this.inx, JSON.stringify(items))
                        resolve(item);
	                } else {
	                    resolve([]);
	                    // Offline.showPopupNoInternet(CloudFaces.Language.translate('need_login'), CloudFaces.Language.translate('no_internet_understand'))
	                }
	        	}).catch(e => {
	        		if (typeof e.code != 'undefined' && e.code == 'no_internet') {
	        			resolve(Offline.get(this.inx));
	        		} else {
	        			// Offline.showPopupNoInternet(CloudFaces.Language.translate('need_login'), CloudFaces.Language.translate('no_internet_understand'))
		        		reject(e);
		        	}
	        	});
	        }
	    });
    },

    getAll: function() {
    	return new Promise((resolve, reject) => {
    		if (!this.needUpdate() && this.storage.valueExists(this.inx) && this.storage.readValue(this.inx)) {
	            let data = JSON.parse(this.storage.readValue(this.inx));
	            if (data.length > 0) {
	                resolve(data);
	            } else {
	                resolve([]);
	            }
	        } else {
	            this.getCMS().then(items => {	        		
	        		if (typeof items != 'undefined' && items.length) {
	        			this.storage.writeValue(this.inx, JSON.stringify(items));
                        Offline.set(this.inx, JSON.stringify(items))
                        resolve(items);
	                } else {
	                    resolve([]);
	                    // Offline.showPopupNoInternet(CloudFaces.Language.translate('need_login'), CloudFaces.Language.translate('no_internet_understand'))
	                }
	        	}).catch(e => {
	        		if (typeof e.code != 'undefined' && e.code == 'no_internet') {
	        			resolve(Offline.get(this.inx));
	        		} else {
	        			// Offline.showPopupNoInternet(CloudFaces.Language.translate('need_login'), CloudFaces.Language.translate('no_internet_understand'))
		        		reject(e);
		        	}
	        	});
	        }
    	});        
    }
};

let Offline = {
    addPopup: false,

    get: function(key) {
        if (CFPersistentStorage.valueExists(key)) {
            // Offline.showPopupNoInternet(CloudFaces.Language.translate('check_internet'), CloudFaces.Language.translate('no_internet_understand'))
            return JSON.parse(CFPersistentStorage.readValue(key));
        }
    },

    set: function(key, value) {
        CFPersistentStorage.writeValue(key, value);
    },

    showPopupNoInternet: function(infotext, textButtn) {
        if (!Offline.addPopup) {
            // var dirPrefix = '/modules/';
            // if (location.pathname.indexOf('modules') >= 0) {
            //     dirPrefix = '../';
            // }
            // $('body').append('<div class="no-internet-popup">' +
            //     '<div class="no-internet-inner">' +
            //     '<div class="no-internet-icon">' +
            //     '<img src="' + dirPrefix + 'appCore/images/warning.png" alt="" />' +
            //     '</div>' +
            //     '<div class="no-internet-warning-text">' +
            //     infotext +
            //     '</div>' +
            //     '<div class="understand">' +
            //     textButtn +
            //     '</div>' +
            //     '</div>' +
            //     '</div>')
            // Offline.addPopup = true;
            // $(document).on('click', '.understand', function() {
            //     $('.no-internet-popup').remove();
            //     Offline.addPopup = false;
            // })
        }
    }

}

export default CFCaching;