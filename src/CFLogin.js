function CFLogin(client, listId) {
    if (!(this instanceof CFLogin)) {
        return new CFLogin(client, listId);
    }

    Object.assign(this, {
        client: client,
        list_id: listId
    })
}

CFLogin.prototype = {
    // updated for relations
    register: function(params) {
        let url = 'project/id/%s/lists/%s/items/'
            .replace('%s', this.client.app_id)
            .replace('%s', this.list_id)
        return this.client.put(url, params);
    },
    // stays the same for relations
    login: function(params) {
        let url = 'project/id/%s/appuser/login/'
            .replace('%s', this.client.app_id)
        return this.client.post(url, params);
    },
    // stays the same for relations
    logout: function() {
        let url = 'project/id/%s/appuser/logout/token/%s/'
            .replace('%s', this.client.app_id)
            .replace('%s', this.client.user_token)
        return this.client.post(url, {});
    },
    // updated for relations
    update: function(id, params) {
        params = typeof params == 'object' ? params : {};
        params.id = id;

        let url = 'project/id/%s/lists/%s/items/'
            .replace('%s', this.client.app_id)
            .replace('%s', this.list_id)
        return this.client.put(url, params);
    },
    // stays the same for relations
    getKey: function(params) {
        let url = 'project/id/%s/appuser/generate/'
            .replace('%s', this.client.app_id)
        return this.client.post(url, params);
    },
    // stays the same for relations
    resetPassword: function(key, params) {
        let url = 'project/id/%s/appuser/reset/key/%s/'
            .replace('%s', this.client.app_id)
            .replace('%s', key)
        return this.client.post(url, params);
    },
    // updated for relations
    profileTemplates: function(listId) {
        let url = 'project/id/%s/lists/%s/items/'
            .replace('%s', this.client.app_id)
            .replace('%s', listId)
        return this.client.get(url, {});
    },
    // updated for relations
    getUserData: function(token) {
        let url = 'project/id/%s/lists/%s/items/where/%s/'
            .replace('%s', this.client.app_id)
            .replace('%s', this.list_id)
            .replace('%s', 'token="' + token + '"');
        return this.client.get(url, {});
    }
}

export default CFLogin;